
// jQuery init
$(document).ready(function () {
    var viewportWidth = $(window).width();
    var viewportHeight = $(window).height();

    // Text Animations


    // Backgrund Animations

    // Particles.init({
    //     selector: '#particles-1',
    //     connectParticles: true,
    //     maxParticles: 30
    //   });
     
    
    // Scroll Ease

    var $window = $(window);
    var scrollTime = 0.42;
    var scrollDistance = 270;

    // prevent default scrolling
    $window.on("mousewheel DOMMouseScroll", function(event){
        event.preventDefault();
        var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
        var scrollTop = $window.scrollTop();
        var finalScroll = scrollTop - parseInt(delta*scrollDistance);

        TweenMax.to($window, scrollTime, {
            scrollTo : { y: finalScroll, autoKill:true },
            ease: Power1.easeOut,
            overwrite: 5
        });
    });


    // Audio

    //create a synth and connect it to the master output (your speakers)
    var synth = new Tone.Synth().toMaster();

    //play a note every quarter-note
    var loop = new Tone.Loop(function(time){
        synth.triggerAttackRelease("C2", "8n", time);
    }, "4n");

    loop.start("1m").stop("4m");
    //Tone.Transport.start();


    // Parallax

    // init controller
    var scrollMagicController = new ScrollMagic.Controller({
        globalSceneOptions: {
            triggerHook: 0,
            reverse: true
        }
    });

    // // create all scenes in page
    // var createScenes = function (sceneIds) {
        
    //     // use a set if element ids to declare each scene
    //     sceneIds.forEach(function(id) {
    //         // elements ids convention
    //         var triggerElement = '#trigger-scene-' + id;
    //         var setPin = '#scene-' + id
    //         // directy into controller
    //         scrollMagicController.addScene(
    //             new ScrollMagic.Scene({
    //                 triggerElement: triggerElement,
    //                 duration: $window.height()/2            
    //             })                
    //             .setPin(setPin)
    //         )
    //     });
    // };

    // var sceneIds = ['a','b','c'];
    //createScenes(sceneIds);

    // timelines

    // nav-bar timeline
    var navBarTimelineA = new TimelineLite(),
        navBarTimelineB = new TimelineLite(),
        navBarTimelineC = new TimelineLite();
    // first presentation timelines
    var sceneATimeline = new TimelineLite(),
        sceneBTimeline = new TimelineLite(),
        sceneCTimeline = new TimelineLite();

    // scenes

    // nav-bar scenes

    var navBar = $('#scene-nav'),
        headerTitle = $('#title-div'),
        headerCommunicationNode = $('#communication-node');
    

    // nav-bar tweens for the timelines
    navBarTimelineA.to(navBar, 0.5, {display:'inline', top: 0, ease: Power2.EaseIn, marginTop:'2%', opacity: 0.5, width:'100%', float:'left'});
    navBarTimelineB.to(headerTitle, 1, {x:-(viewportWidth*0.33)});
    navBarTimelineB.to(headerTitle, 0, {position:'fixed'}).set("#communication-node", {text: "meetings for flexible people"});    
    navBarTimelineB.to(headerCommunicationNode, 0.7, {opacity: 1, ease: Power2.EaseIn});
    navBarTimelineC.to(headerCommunicationNode, 0.5, {opacity: 0, ease: Power2.EaseIn});
    navBarTimelineC.to(headerTitle, 0, {position:'fixed'}).set("#communication-node", {text: "like you?"});
    navBarTimelineC.to(headerCommunicationNode, 0.5, {autoAlpha: 1, ease: Power2.EaseIn});

    // create nav-bar scenes

    var headerScene1 = new ScrollMagic.Scene({
        triggerElement: '#trigger-scene-nav',
        triggerHook: 'onCenter'
    })        
    .setPin('#step-1')
    .setClassToggle(navBar[0], 'fixed')
    .setTween(navBarTimelineA);

    var headerScene2 = new ScrollMagic.Scene({
        triggerElement: '#trigger-step-0',
        triggerHook: 'onCenter'
    })        
    .setPin('#scene-nav-div')
    .setTween(navBarTimelineB);

    var headerScene3 = new ScrollMagic.Scene({
        triggerElement: '#trigger-step-1',
        triggerHook: 'onCenter'
    })        
    .setPin('#pin-this')
    .setTween(navBarTimelineC);

    var headerRemove = new ScrollMagic.Scene({
        triggerElement: '#trigger-step-2',
        triggerHook: 'onCenter'
    }).on('start', function () {
        anime({
            targets: '#scene-nav',
            opacity: 0,
            duration: 300,
            easing: 'linear',
            complete: function () {
                // navBar.removeClass('fixed');
                // navBar.addClass('remove');
            }
        });
    });

    
            
    
    // scene-a, show only after nav-bar tweens completed    
    
    // scene-a tweens for the timelines
    var sceneAQuestion = $('#scene-a-question');
    var sceneATextA = $('#scene-a-text-a');
    var sceneAImgA = $('#scene-a-img-a');
    var sceneATextB = $('#scene-a-text-b');
    var sceneAImgB = $('#scene-a-img-b');
    var sceneATextC = $('#scene-a-text-c');
    var sceneAImgC = $('#scene-a-img-c');
    
    sceneATimeline
        .to(sceneAQuestion, 1, {opacity:1})
        .to(sceneATextA, 0.4, {delay:3, opacity:1, rotation:5})
        .add(function () {
            // textillate
        })
        .to(sceneAImgA, 0.4, {opacity:1})
        .to(sceneAImgA, 0.4, {css: {className:'bounce-it'}, }, '-=0.4')
        .to(sceneAImgB, 0.4, {opacity:1})
        .to(sceneAImgB, 0.4, {css: {className:'bounce-it'}, }, '-=0.4')
        .to(sceneATextB, 0.4, {opacity:1, rotation:-5})
        .to(sceneATextC, 0.4, {opacity:1})
        .to(sceneAImgC, 0.4, {opacity:1})
        .to(sceneAImgC, 0.4, {css: {className:'bounce-it'}, }, '-=0.4')
    

    var sceneA = new ScrollMagic.Scene({
            triggerElement: '#trigger-scene-a',
            duration: $window.height(),
            triggerHook: 'onLeave'  
        })                
        .setPin('#scene-a')
        .setTween(sceneATimeline);
    
    // scene-b
    
    // scene-b tweens for the timelines
    //sceneATimeline.to();

    var sceneB = new ScrollMagic.Scene({
        triggerElement: '#trigger-scene-b',
        duration: $window.height()          
    })                
    .setPin('#scene-b');

    // scene-c
    
    // scene-c tweens for the timelines
    //sceneATimeline.to();

    var sceneC = new ScrollMagic.Scene({
        triggerElement: '#trigger-scene-c',
        duration: $window.height()            
    })                
    .setPin('#scene-c');

    // add all scenes to controller
    scrollMagicController.addScene([
        headerScene1,
        headerScene2,
        headerScene3,
        headerRemove,
        sceneA,
        sceneB,
        sceneC,
    ]);
    
});

